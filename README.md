# Forschungspraktikum 
Das folgende Repository beinhaltet einen Chatbot, welcher nach den [Openaire Guidelines][OpenAire] eine Metadatenbeschreibung erstellt, welche im XML Format exportiert wird. 
Es liegen eine Webanwendung in [React][react] sowie einen virtuellen Assistenten vor, welcher mit dem [Rasa][rasa] Framework erstellt wurde. 
Das Ziel ist eine einfache und intuitive Erstellung von einem Metadatenset, welcher eine alternative zu bereits bestehenden, formularbasierten Ansätzen darstellt. Ein Beispiel wäre der [DublinCore Generator][DC]
![](example.gif)

## Features
- Webdesign in React
- Rasa Assistent mit trainiertem Model

## Installation
B.I.R.D benötigt zur Anwendung folgende Dependencies:
- [NodeJS][node]
- [ReactJS][react]
- [Rasa Open Source][rasa_install]

Installiere die Dependencies und führe folgendes aus. Nutze dabei je einzelne Tabs, insgesamt werden 3 Services benötigt (Frontend, Rasa Server, Rasa Action Server)
### React Frontend
```sh
cd web
npm i
npm start
```
### Rasa installation
```sh
cd rasa
sudo apt update
sudo apt install python3-dev python3-pip
python3 -m venv ./venv
source ./venv/bin/activate
pip3 install -U pip
pip3 install rasa
rasa train -d domain
```
### Rasa Backend - Rasa Server
```sh
cd rasa 
rasa run --connector socketio --cors "*"
```
### Rasa Backend - Rasa Action Server
```sh
cd rasa
rasa run actions --cors '*'
```

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [OpenAire]: <https://guidelines.openaire.eu/en/latest/data/index.html>
   [react]: <https://reactjs.org/>
   [rasa]: <https://rasa.com/>
   [dc]: <https://nsteffel.github.io/dublin_core_generator/>
   [node]: <https://nodejs.org/en/>
   [rasa_install]: <https://rasa.com/docs/rasa/installation/>
   

