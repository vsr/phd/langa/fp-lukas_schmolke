import logo from './bird.svg';
import './App.css';
import {Widget} from 'rasa-webchat';
import { useEffect, useState, useRef} from 'react';
import {XMLForm} from './components/XMLForm'

//import {Widget} from './webchat/index'
function App() {
  //currently, the exported xml is saved in a xml State, every line is a index in a array
  //TODO: useful datastructure!
  const [xml, setXml] = useState("<?xml version='1.0' encoding='UTF-8' ?> <resource> Hier entsteht xml</resources>");
  return (
    <div className="App">
      <header className="App-header">
        <h1>B.I.R.D.</h1>
        <hr />
        <h3>Generate your own XML Metadata for data archives!</h3>
        <img src={logo} className="App-logo" alt="logo" />
      </header>
    <div className="main">
    <div className="xml">
        <p>
          hier entsteht deine Beschreibung für dein Archiv!
        </p>
        <XMLForm
          xml={xml}
          setXml={setXml}
        />
    </div>
      <div className="chat">
        <Widget
          //modified widget, original is actually not included in git, lib is currently packed by webpack
          xml={xml}
          setXml={setXml}
          title={"BIRD"}
          initPayload={"/start_form"}
          //socketUrl={"vsrstud02.informatik.tu-chemnitz.de:5005"}
          socketUrl={"http://localhost:5005"}
          socketPath={"/socket.io"}
          embedded={true}
        />
      </div>
    </div>

  </div>
  );
}

export default App;
