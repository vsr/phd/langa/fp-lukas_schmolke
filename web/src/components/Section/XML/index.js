import Form from 'react-bootstrap/Form'
import './style.css'
import beautify from 'xml-beautifier';
const XmlBeautify = require('xml-beautify');
const { DOMParser } = require('xmldom');

export default function XML(props){

    function markAll(e){
        e.target.select();
    }
    function handleChange(e){
        
    }
    function createDownload(){
        const element = document.createElement("a");
        const file = new Blob([xmlToString()], {type: 'text/plain'});
        element.href = URL.createObjectURL(file);
        element.download = "myFile.xml";
        document.body.appendChild(element); // Required for this to work in FireFox
        element.click();
      }

    function xmlToString()
    {
        var help = String(props.xml);
        console.log(help)
        // var formatted = beautify(help);
        const beautifiedXmlText = new XmlBeautify({ parser: DOMParser }).beautify(help);
        // var xml = props.xml.join('\n')
        return beautifiedXmlText;
    }
    return(
        <div className="section gray" id="howto" >
            <div className="header">
            <h2>Output</h2>
            <hr />
            </div>
            <div className="content">
            Hier kannst du deinen generierten Metatag einfach und bequem herauskopieren!
                <div className="xml-viewer">
                    <textarea class="d-flex p-2 bd-highlight form-control form-xml" id="exampleFormControlTextarea1" rows="14" onClick={markAll} value={xmlToString()}></textarea>
                </div>
                <button className="download-button" onClick={createDownload}>Download Datei</button>
            </div>
        </div>
    )
}