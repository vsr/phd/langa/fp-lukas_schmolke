import Widget from 'rasa-webchat';
import "./style.css"
import {animateScroll as scroll } from "react-scroll";
import {useState, useEffect} from 'react'; 


export default function Chat(props){
    const [offline, setOffline] = useState(true);
    
    const removeLocalStorage = () => {
        
        localStorage.removeItem('chat_session');
        window.location.reload(); 
    }

    const delayFunction = (message) => {

        let delay = message.length * 20;
        if (delay > 2 * 1000) delay = 2 * 1000;
        //let delay = 0;
        return delay;
    }
    
    const scrollToTop = () => {
        scroll.scrollToTop();
    };
    useEffect(() => {
        scrollToTop();
        console.log(props.xml)
    }, [Widget])
    
    const showBlinking = () => {
        if(offline)
        {
            return <circle cx="25" cy="25" r="6" fill="red" />
        }
        else
        {
            return <circle cx="25" cy="25" r="6" fill="red" />
        }
    }
    return(
        <div className="section blue chatting" id="howto" >
            <div className="header">
            <h2>B.I.R.D</h2>
            <hr />
            </div>
            <div className="content">
            <div className="chatHeader" >
                B.I.R.D - Livechat
                <svg height="60" width="100" class="blinking">
                {offline && <circle cx="30" cy="30" r="6" fill="red" />}
                {!offline && <circle cx="30" cy="30" r="6" fill="lightgreen" />}
                Sorry, your browser does not support inline SVG.  
</svg> 
            </div>
            <div className="chat">
                <Widget
                //modified widget, original is actually not included in git, lib is currently packed by webpack
                xml={props.xml}
                setXml={props.setXml}
                title="BIRD"
                initPayload="/start_form"
                socketUrl={"vsrstud02.informatik.tu-chemnitz.de:5005"}
                //socketUrl={"http://localhost:5005"}
                socketPath={"/socket.io"}
                embedded={true}
                showMessageDate={true}
                hideWhenNotConnected={false}
                customMessageDelay={delayFunction}
                params={{
                    images: {
                    dims: {
                        width: 400
                    }
                    }
                }}
                onSocketEvent={{
                'connect': () => setOffline(false),
                'disconnect': () => setOffline(true)
                }}
                />
            </div>
            <button onClick={removeLocalStorage}>Lösche Chatverlauf</button>
            </div>
        </div>
    )
}
