//import './style.css'
import Carousel from 'react-bootstrap/Carousel';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import { Link, animateScroll as scroll } from "react-scroll";

export default function Imprint(){
    return(
        <div className="section green" id="imprint" >
            <div className="header">
            <h1>Impressum</h1>
            <hr />

            </div>
            <div className="content">
	    <h3>Anbieterkennzeichnung</h3>

		<p>Diese Webseite wird bereitgestellt von der</p>
		<p>
			Technischen Universität Chemnitz<br/>
			Professur Verteilte und Selbstorganisierende Rechnersysteme<br/>
			Dipl-Inf. André Langer, Lukas Schmolke B.Sc.<br/>
			Straße der Nationen 62<br/>
			09111 Chemnitz<br/>
			Deutschland<br/>
			Telefon: +49 371 531-30469<br/>
			Telefax: +49 371 531-830469<br/>
			E-Mail: andre.langer@informatik.tu-chemnitz.de<br/>
			Web: vsr.informatik.tu-chemnitz.de
		</p>
		<p>in Ergänzung zum <a href="https://www.tu-chemnitz.de/tu/impressum.html">Impressum der TU Chemnitz</a>.</p>
            </div>
        </div>
    )
}