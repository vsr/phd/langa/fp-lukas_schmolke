import './style.css'
import Background from './../../Background'
export default function HowTo(){

    return(
        <div className="section blue" id="howto" >
            <div className="header">
            <h1>Kurzanleitung des Chatbots</h1>
            <hr />
            </div>
            <div className="content">
                Normalerweise benötigst du für diesen Chatbot keinerlei Hilfestellung, da dieser geführt abläuft. Jedoch gibt es 
                besondere Situationen, wie Tippfehler oder der Wunsch nach einen Neustart. Und genau dafür gibt es Kurzbefehle, welche 
                hier aufgeführt werden sollen.
                <br />
                <br />
                <h3>Hauptfunktionen</h3>
                <table class="tg" border="1">
                <thead>
                <tr>
                    <th class="tg-1wig">Befehl</th>
                    <th class="tg-1wig">Beschreibung</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="tg-0lax">Hi</td>
                    <td class="tg-0lax">Starte eine Konversation</td>
                </tr>
                <tr>
                    <td class="tg-0lax">Neustart</td>
                    <td class="tg-0lax">Beginne komplett von vorne!</td>
                </tr>
                <tr>
                    <td class="tg-0lax">Hilfe</td>
                    <td class="tg-0lax">Gibt eine Erklärung zur aktuellen Abfrage aus</td>
                </tr>
                <tr>
                    <td class="tg-0lax">Zurück</td>
                    <td class="tg-0lax">Springt zur letzten Eingabe zurück</td>
                </tr>
                <tr>
                    <td class="tg-0lax">Erkläre "Bezeichnung"</td>
                    <td class="tg-0lax">Liefert Erkläung zu einer Bezeichnung</td>
                </tr>
                </tbody>
                </table>
                <br />  
                <br />
                Natürlich musst nicht penibel darauf geachtet werden, dass immer der genaue Wortlaut getroffen wird, 
                geläufige Synonyme funktionieren natürlich ebenfalls. Dies dient nur als Richtlinie
            </div>
        </div>
    )
}