//import './style.css'
import Carousel from 'react-bootstrap/Carousel';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import { Link, animateScroll as scroll } from "react-scroll";

export default function DPstatement(){
    return(
        <div className="section green" id="dpstatement" >
            <div className="header">
            <h1>Ergänzende Datenschutzerklärung</h1>
            <hr />

            </div>
            <div className="content">


<h3 class="h3 text-xl">Zur Datenverarbeitung durch die Webanwendung B.I.R.D.</h3><br/>
    <p>Vielen Dank, dass Sie die Webanwendung B.I.R.D nutzen wollen. Datenschutz und Datensicherheit haben bei uns oberste Priorität. Diese Datenschutzerklärung klärt Sie in Ergänzung zu den auch insoweit zu beachtenden allgemeinen Hinweisen über die Datenverarbeitung durch die Technische Universität Chemnitz <a href="https://www.tu-chemnitz.de/tu/datenschutz.html" target="new">(https://www.tu-chemnitz.de/tu/datenschutz.html)</a> über Art, Umfang und Zweck der Verarbeitung von personenbezogenen Daten innerhalb der angebotenen Webanwendung der mit ihr verbundenen Webseite, Funktionen und Inhalte auf. </p>
    <p>Aus Gründen der besseren Lesbarkeit wird in dieser Datenschutzerklärung auf die Verwendung geschlechtsspezifischer Sprachformen verzichtet. Sämtliche Personenbezeichnungen gelten gleichwohl für alle Geschlechter.</p>

    <ol class="list-decimal mb-3 mt-0 leading-loose pl-10">
        <h2 class="h2"><li>Verantwortlicher und Ansprechpartner</li></h2>


        <p>Der Verantwortliche im Sinne der EU-Datenschutzgrundverordnung (DSGVO) und anderer nationaler Datenschutzgesetze der Mitgliedsstaaten sowie sonstiger datenschutzrechtlicher Bestimmungen ist die:</p>
        <p>Technische Universität Chemnitz<br/>
            vertreten durch den Rektor: Prof. Dr. Gerd Strohmeier<br/>
            Straße der Nationen 62<br/>
            09111 Chemnitz, Deutschland<br/>
            E-Mail: <a href="mailto:rektor@tu-chemnitz.de">rektor@tu-chemnitz.de</a><br/>
            Telefon: +49 371 531-10000<br/>
            Telefax: +49 371 531-10009<br/>
            Web: <a href="https://www.tu-chemnitz.de">www.tu-chemnitz.de</a></p>

        <p>Die Verantwortliche wird nachfolgend „TUC“ genannt. </p>

        <p>Der inhaltliche Betreiber inkl. der technischen Umsetzung und Verantwortliche der Website sowie Ihr direkter Ansprechpartner in allen datenschutzrechtlichen Fragen ist:  </p>
        <p>Technische Universität Chemnitz<br/>
           Professur für Verteilte und Selbstorganisierende Rechnersysteme<br/>
	   Dipl.-Inf. André Langer<br/>
            Straße der Nationen 62, Gebäude A11.203<br/>
            09111 Chemnitz, Deutschland<br/>
            E-Mail: <a href="mailto:andre.langer@informatik.tu-chemnitz.de">andre.langer@informatik.tu-chemnitz.de</a><br/>
            Telefon: +49 371 531-30469 <br/>
            Telefax: +49 371 531-830469<br/>
            Web: <a href="https://vsr.informatik.tu-chemnitz.de/people/andrelanger">vsr.informatik.tu-chemnitz.de/people/andrelanger</a></p>

        <h2 class="h2"><li>Kontaktdaten des Datenschutzbeauftragten</li></h2>
        <p>Der Datenschutzbeauftragte der TUC ist:</p>
        <p>Gernot Kirchner <br/>
            Datenschutzbeauftragter der TU Chemnitz  <br/>
            Straße der Nationen 62<br/>
            09111 Chemnitz, Deutschland<br/>
            E-Mail: <a href="mailto:datenschutzbeauftragter@tu-chemnitz.de">datenschutzbeauftragter@tu-chemnitz.de</a><br/>
            Telefon: +49 371 531-12030 <br/>
            Telefax: +49 371 531-12039<br/>
            Web: <a href="https://www.tu-chemnitz.de/rektorat/dsb/">www.tu-chemnitz.de/rektorat/dsb/</a></p>
    </ol>

<p>Aktualität und Änderung dieser Datenschutzerklärung</p>

<ol class="list-decimal mb-3 mt-0 leading-loose pl-10">
        <p>Diese Datenschutzerklärung ist aktuell gültig und hat den Stand <strong>April 2021</strong>. Durch die Weiterentwicklung unserer Website und unserer Angebote oder aufgrund geänderter gesetzlicher beziehungsweise behördlicher Vorgaben kann es notwendig werden, diese Datenschutzerklärung zu ändern. Die jeweils aktuelle Datenschutzerklärung kann jederzeit auf der Website unter <a href="https://vsrstud02.informatik.tu-chemnitz.de" target="_blank">https://vsrstud02.informatik.tu-chemnitz.de</a> abgerufen und ausgedruckt werden. Ältere Versionen können auf Anfrage ausgehändigt werden.</p>
</ol>


            </div>
        </div>
    )
}