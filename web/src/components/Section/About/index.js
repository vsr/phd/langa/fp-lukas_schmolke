//import './style.css'
import Carousel from 'react-bootstrap/Carousel';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import { Link, animateScroll as scroll } from "react-scroll";

export default function About(){
    return(
        <div className="section green" id="about" >
            <div className="header">
            <h1>Über B.I.R.D.</h1>
            <hr />

            </div>
            <div className="content">
            <h3>Was sind Metadaten?</h3>

                Allein im Jahr 2020 wurden bei Github über 60 Millionen Repositories eröffnet <a href="https://octoverse.github.com/">[1]</a>. 
                Es gibt also eine gigantische Anzahl an Informationen und Knowledge Base in jedem erdenklichen Bereich. Und dieses Wissen ist stetig am
                wachsen. Gerade bei diesen steigenden Zahlen ist es umso wichtiger, dass Wissen hierbei nicht verloren geht. Das Stichwort lautet <i>Metadaten</i>,
                das sind <b>strukturierte Daten</b>, die Informationen über andere Daten enthalten. Ein analoges Beispiel wäre für ein Buch der Name des Autors, die Auflage,
                das Erscheinungsjahr, Verlag oder ISBN. Dies lässt sich auch auf Datenarchive anwenden, um diese besser im Netz zu einer thematisch passenden Suche aufzufinden 
                und semantisch zu verknüpfen. 
                <br />
                <Carousel fade="true" interval="50000000" className="justify-content-md-center">
                    <Carousel.Item>
                    <img
                        className="d-block"
                        src="./metadata_comic.jpg"
                        alt="First slide"
                        />
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                        className="d-block"
                        src="./metadata_2.jpg"
                        alt="Second slide"
                        />

                    </Carousel.Item>
                    {/* <Carousel.Item>
                        <img
                        className="d-block w-100"
                        src="holder.js/800x400?text=Third slide&bg=20232a"
                        alt="Third slide"
                        />

                        <Carousel.Caption>
                        <h3>Third slide label</h3>
                        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                        </Carousel.Caption>
                    </Carousel.Item> */}
                    </Carousel>
                    <br />
                <h3>Was macht B.I.R.D.?</h3>
                Damit Metadaten für einen Computer einfach zu verarbeiten sind, sind diese leider nicht für jeden einfach zu verstehen, und gleich recht nicht zu generieren. 
                Genau hier setzt B.I.R.D an, ein <b>interaktiver Chatbot</b>, welcher ein einfaches Metadatenset für dein Datenarchiv erstellt! Du musst nur die gestellten Fragen
                beantworten, und der rest wird automatisch erledigt 🧙🏻. Toll oder? Wenn du auch der Meinung bist, dann kannst du direkt loslegen!
                <br />
                <br />
                <li className="section-li">
                <Link 
                    activeClass="active"
                    to="bot"
                    spy={true}
                    smooth={true}
                    offset={-50}
                    duration={500}
                    >
                    Los gehts!
                    </Link> 
                </li>



            </div>
        </div>
    )
}