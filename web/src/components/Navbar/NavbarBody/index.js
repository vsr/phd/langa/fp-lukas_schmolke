import React, { Component } from "react";
import logo from "../../../bird.svg";
import Particles from 'react-particles-js';

export default function NavbarBody(){

    return (
        <div className="navheader" id="navheader">
                  <div class="bg"></div>
        <div class="bg bg2"></div>
        <div class="bg bg3"></div>
        <header className="App-header">

        <h1>B.I.R.D.</h1>
        <hr />
        <h3>Generate your own XML Metadata for data archives!</h3>
        <img src={logo} className="App-logo" alt="logo" />
        </header>
        {/* <Background id="navheader" /> */}
      </div>
    )
}