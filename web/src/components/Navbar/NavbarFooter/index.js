import React, { Component } from "react";
import { Link, animateScroll as scroll } from "react-scroll";
import logo from "../../../bird.svg";
import { useEffect, useState, useRef} from 'react';
import './style.css'


export default function NavbarFooter(){
    const [help, setHelp] = useState(0)
    const [height, setHeight] = useState(0)
    const [fixed, setFixed] = useState(false)
    const scrollToTop = () => {
        scroll.scrollToTop();
    };

    const handleScroll = () => {
        setHelp(window.pageYOffset)
        localStorage.setItem("pos", window.pageYOffset)
        setHeight(document.getElementById('navheader').clientHeight)
    }

    const handleResize = () => {
        setHeight(document.getElementById('navheader').clientHeight)
    }

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        window.addEventListener('resize', handleResize);
        setHeight(document.getElementById('navheader').clientHeight)
        console.log(document.getElementById('navheader').clientHeight)
        scrollToTop();
    }, [])

    useEffect(() => {
        if(help > height)
        {
            setFixed(true)
        }
        else
        {
            setFixed(false)
        }
    })

    
    return (
        <div >
        <div className={(fixed ? "placeholder" : "")}/>
        <ul className={"nav-items" + (fixed ? " fixed" : "")}>
        <li className="nav-item" onClick={scrollToTop}>
                    <img
                        src={logo}
                        className="nav-logo"
                        alt="Logo"
                    />
                </li>
                {/* <li className="nav-item">
                    <h3>{Math.round(help)} </h3>
                    <h3>{height}</h3>
                </li> */}
                <li className="nav-item">
                    <Link 
                        activeClass="active"
                        to="about"
                        spy={true}
                        smooth={true}
                        offset={-50}
                        duration={500}
                    >
                    Was ist das?
                    </Link>
                </li>
                <li className="nav-item">
                    <Link 
                        activeClass="active"
                        to="howto"
                        spy={true}
                        smooth={true}
                        offset={-50}
                        duration={500}
                    >
                    Wie Funktionierts?
                    </Link>
                </li>
                <li className="nav-item">
                    <Link 
                        activeClass="active"
                        to="bot"
                        spy={true}
                        smooth={true}
                        offset={-50}
                        duration={500}
                    >
                    <b>B.I.R.D. </b>
                    </Link>
                </li>
                <li className="nav-item">
                    <Link 
                        activeClass="active"
                        to="output"
                        spy={true}
                        smooth={true}
                        offset={-50}
                        duration={500}
                    >
                    Mein Output
                    </Link>
                </li>
                <li className="nav-item">
                    <Link 
                        activeClass="active"
                        to="imprint"
                        spy={true}
                        smooth={true}
                        offset={-50}
                        duration={500}
                    >
                    Impressum
                    </Link>
                </li>
                <li className="nav-item">
                    <Link 
                        activeClass="active"
                        to="dpstatement"
                        spy={true}
                        smooth={true}
                        offset={-50}
                        duration={500}
                    >
                    Datenschutzerklärung
                    </Link>
                </li>
            </ul>
        </div>
    )
}
