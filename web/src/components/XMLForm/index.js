import Form from 'react-bootstrap/Form'
import './style.css'
import beautify from 'xml-beautifier';
const XmlBeautify = require('xml-beautify');
const { DOMParser } = require('xmldom');

export function XMLForm(props){

    function markAll(e){
        e.target.select();
    }
    function handleChange(e){
        
    }

    function xmlToString()
    {
        var help = String(props.xml);
        console.log(help)
        return(help)
        // var formatted = beautify(help);
        // const beautifiedXmlText = new XmlBeautify({ parser: DOMParser }).beautify(help);
        // // var xml = props.xml.join('\n')
        // return beautifiedXmlText;
    }
    return(
        <div className="xml-viewer">
            <textarea class="d-flex p-2 bd-highlight form-control form-xml" id="exampleFormControlTextarea1" rows="14" onClick={markAll} value={xmlToString()}></textarea>
        </div>
    )
}