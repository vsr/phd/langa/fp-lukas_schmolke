import logo from './bird.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import {Widget} from 'rasa-webchat';
import { useEffect, useState, useRef} from 'react'; 
import { Link, animateScroll as scroll } from "react-scroll";

import {XMLForm} from './components/XMLForm'
import Navbar from './components/Navbar'
import Section from './components/Section'
import HowTo from './components/Section/HowTo'
import About from './components/Section/About'
import Background from './components/Background'
import Chat from './components/Section/Chat'
import DPstatement from './components/Section/DPstatement'
import Imprint from './components/Section/Imprint'
import XML from './components/Section/XML'

function App() {

  const [xml, setXml] = useState("<?xml version='1.0' encoding='UTF-8' ?> <resource> Hier entsteht xml</resources>");
  document.title = "Metadata Generator"
  
  // when all components are loaded, scroll back to old position, saved in localStorage item pos
  // this element gets changed in the scroll event, triggered in the NavbarFooter index.js
  useEffect(() => {
    scroll.scrollTo(localStorage.getItem("pos"))
  }, [])

  return (
    <div className="App">
      <Navbar />
      <div id="sections" className="sections">
      <About />
      <HowTo />
      <div  id="bot">
      <Chat
          xml={xml}
          setXml={setXml}
        />
      </div>
      <div id="output">
        <XML 
          xml={xml}
        />
      </div>
        <Imprint />
        <DPstatement />
      </div>
    </div>
  );
}

export default App;
