const { Component } = require("react");


function printer(){
    console.log("test function to export")
}

function Test(props){

    const {name, age} = props

    return <div>
        <p>{name}</p>
        <p>Age: {age}</p>
    </div>
}

export {
    Test,
    printer
}