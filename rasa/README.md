# Rasa repository
in diesem Repository liegen die Rasa Daten für BIRD
## Aufbau
Die Ordnerstruktur besitzt folgenden Aufbau

```
actions                                 # contains all custom python action files
    | <formName>Actions.py
data                                    # contains all NLU and Responses
    | -- nlu
        | -- responses
            | <FormName>Response.yml
        | <formName>NLU.yml
        | generalNLU.yml                # general NLU examples
    | -- rules
        | <formName>Form.<ml
        | general.yml                   # general rules
    | -- stories
        | <FormName>Stories.yml
        | general.yml                   # general stories
domain
    | <FormName>Domain.yml
    | domain.yml                        # general domain settings
```

Dieses Pattern zieht sich durch die komplette Anwendung:
wenn weitere Informationen hinzugefügt werden sollen, so wird für jede Gruppe an Informationen eine neue Domain, rule, story, NLU, response und ggf. auch eine custom action hinzugefügt.


## Serveranbindung
für das Deployment müssen je nach Nutzungstyp die _credentials.yml_ und _endpoints.yml_ mit korrekten Port und IP Adresse angepasst werden.