from typing import Text, List, Any, Dict

from rasa_sdk import Tracker, FormValidationAction, Action
from rasa_sdk.executor import CollectingDispatcher
from typing import Text, List, Optional
from rasa_sdk.events import EventType, SlotSet
from rasa_sdk.types import DomainDict
from rasa_sdk.forms import FormValidationAction

from typing import Dict, Text, List, Optional, Any

from .XMLHelper import XMLHelperClass as xmlh
from .identifierHelper import identifierHelperClass as idh
import xml.etree.ElementTree as ET

import copy

class ActionTestSubmitNames(Action):
    def name(self) -> Text:
        return "action_submit_names"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        """
        Submit function for generating creator entities
        """

        #get name
        name = tracker.slots.get("name")
        dispatcher.utter_message(text=f"Name: {name}")

        #xml
        xml = tracker.slots.get("xml")

        if xml == None:
            root = xmlh.generateRootXML()
            creators = ET.SubElement(root, "creators")
            tree = ET.ElementTree(root)
        else:
            root = ET.fromstring(xml)
        
        creators = root.find("creators")

        creator = None

        if creators is None:
            creators = ET.SubElement(root, "creators")

        creator = xmlh.findElementWithValue(root, name, "creatorName")
        if creator == None:
            creator = ET.SubElement(creators, "creator")
            creatorName = ET.SubElement(creator, "creatorName").text = name

            if tracker.get_slot("isNameIdentifier"):
                IdType = tracker.slots.get("nameIdentifierType")
                Id = tracker.slots.get("nameIdentifier")
                #if https:// or http:// prefix, cut this away
                if Id.startswith("https://") or Id.startswith("http://"):
                    helper = Id.split('/')
                    Id = "".join(helper[3:])

                dispatcher.utter_message(text=f"{IdType} : {Id}")

                if IdType.upper() == "ISNI":
                    nameIdentifier = ET.SubElement(creator, "nameIdentifier", nameIdentifierScheme=IdType, schemeURI="http://www.isni.org").text = Id
                elif IdType.upper() == "ORCID":
                    nameIdentifier = ET.SubElement(creator, "nameIdentifier", nameIdentifierScheme=IdType, schemeURI="http://orcid.org").text = Id

        root2 = copy.deepcopy(root)
        test = xmlh.ExportXML(root2)
        t = xmlh.prettify(test)

        xmlTest = {"data" : { "xml" : ET.tostring(test).decode()}}
        dispatcher.utter_custom_json(xmlTest)

        xml = ET.tostring(root).decode()

        return [SlotSet("name", None), SlotSet("xml", xml)]


class ResetNamesAction(Action):
    def name(self) -> Text:
        return "action_reset_names"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        """
        helper function for reset all entities for creator, to add a new one
        """

        #resets the name form
        return [SlotSet("nameIdentifierType", None), SlotSet("nameIdentifier", None), SlotSet("isNameIdentifier", None), SlotSet("name", None)]


class ValidateNames(FormValidationAction):
    def name(self) -> Text:
        return "validate_names_form"
        

    async def required_slots(
        self,
        slots_mapped_in_domain: List[Text],
        dispatcher: "CollectingDispatcher",
        tracker: "Tracker",
        domain: "DomainDict",
    ) -> Optional[List[Text]]:
        
        additional_slots = []
        
        if tracker.slots.get("isNameIdentifier"):

            additional_slots.append("nameIdentifierType")
            additional_slots.append("nameIdentifier")
        return additional_slots + slots_mapped_in_domain
        

    def validate_nameIdentifierType(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """
        validation for nameidentifierType
        """

        if tracker.latest_message.get("intent")["name"] in ["question", "stop", "change", "revert"]:
            intent =  tracker.latest_message.get("intent")["name"]
            if intent == "revert":
                dispatcher.utter_message(text=f"Alles klar, ich setze den gewählten Identifikationstyp {slot_value} zurück.")
                return {"nameIdentifierType" : None, "isNameIdentifier" : None}

            if intent == "question":
                dispatcher.utter_message(template="utter_explain/nameIdentifierType")
            
            return {"nameIdentifierType" : None}
        else:
            return {"nameIdentifierType": slot_value}


    async def extract_nameIdentifierType(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        """
        Extractor function for nameIdentifierType
        """

        if tracker.get_slot("requested_slot") == "nameIdentifierType":
            answer = next(tracker.get_latest_entity_values("nameIdentifierType"), None)
            
            if answer != None:
                return {"nameIdentifierType": answer}
            else:
                return {"nameIdentifierType" : None}


    def validate_nameIdentifier(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """
        validator function for nameIdentifier
        """

        if tracker.latest_message.get("intent")["name"] in ["question", "stop", "change", "revert"]:
            intent =  tracker.latest_message.get("intent")["name"]
            if intent == "revert":
                dispatcher.utter_message(text=f"Alles klar, ich setze den Identifikator {slot_value} zurück.")
                return {"nameIdentifier" : None}

            if intent == "question":
                dispatcher.utter_message(template="utter_explain/nameIdentifier")
            
            return {"nameIdentifier" : None}
        else:
            # get nameIdentifierType
            typ = tracker.slots.get('nameIdentifierType')
            if typ.lower() == "orcid":
                slot_value = slot_value.replace(" ", "-")
                name = idh.get_ORCID(slot_value)
            elif typ.lower() == "isni":
                slot_value = slot_value.replace("-", " ")
                name = idh.get_ISNI(slot_value)

            if name:
                dispatcher.utter_message(text=f"Super, ich habe {name} gefunden!")
                return {"nameIdentifier": slot_value, "name" : name}
            else:
                dispatcher.utter_message(text=f"Entschuldigung, aber ich glaube du hast dich vertippt. Ich kann leider keine Person mit der id {slot_value} finden")
                return {"nameIdentifier" : None}
        

    async def extract_nameIdentifier(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        """
        Extractor function for nameIdentifier
        """

        if tracker.get_slot("requested_slot") == "nameIdentifier":

            entities = tracker.latest_message.get("entities")
            answer = None
            
            # extract regex entity identifier_id
            if any(entity['entity'] == 'identifier_id' for entity in entities):
                
                for entity in entities:
                    # if entity["extractor"] == 'RegexEntityExtractor':
                    answer = entity["value"]

            if answer is None:
                answer = tracker.latest_message.get("text")

            if answer != None:
                return {"nameIdentifier": answer}
