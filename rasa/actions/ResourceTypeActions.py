from typing import Text, List, Any, Dict

from rasa_sdk import Tracker, FormValidationAction, Action
from rasa_sdk.executor import CollectingDispatcher
from typing import Text, List, Optional
from rasa_sdk.events import EventType, SlotSet
from rasa_sdk.types import DomainDict
from rasa_sdk.forms import FormValidationAction

from typing import Dict, Text, List, Optional, Any

from .XMLHelper import XMLHelperClass as xmlh
import xml.etree.ElementTree as ET

import copy

class ActionSubmitSize(Action):
    def name(self) -> Text:
        return "action_submit_resourceType"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        """
        Function for generating XML Code for all resource entities
        """

        #get slots
        resourceType = tracker.slots.get("resourceType")
        resourceTypeDescription = tracker.slots.get("resourceTypeDescription")
        
        #xml
        xml = tracker.slots.get("xml")
        root = ET.fromstring(xml)
        
        resourceTypeXML = xmlh.findElementWithValue(root, resourceTypeDescription, "resourceType")
        if resourceTypeXML == None:
            resourceTypeXML = ET.SubElement(root, "resourceType", resourceTypeGeneral=str(resourceType)).text = resourceTypeDescription
 
        root2 = copy.deepcopy(root)
        test = xmlh.ExportXML(root2)
        t = xmlh.prettify(test)

        xml = ET.tostring(root).decode()
        
        xmlTest = {"data" : { "xml" : ET.tostring(test).decode()}}
        dispatcher.utter_custom_json(xmlTest)
        
        dispatcher.utter_message(text=f"Du hast als Resourcentyp {resourceType} mit der Beschreibung {resourceTypeDescription} gewählt!")

        return [SlotSet("xml", xml)]
