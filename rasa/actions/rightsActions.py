from typing import Text, List, Any, Dict

from rasa_sdk import Tracker, FormValidationAction, Action
from rasa_sdk.executor import CollectingDispatcher
from typing import Text, List, Optional
from rasa_sdk.events import EventType, SlotSet
from rasa_sdk.types import DomainDict
from rasa_sdk.forms import FormValidationAction

from typing import Dict, Text, List, Optional, Any

from .XMLHelper import XMLHelperClass as xmlh
import xml.etree.ElementTree as ET

import copy
import requests
import json
import urllib.parse

class ActionXMLDescriptionSubmitter(Action):
    def name(self) -> Text:
        return "action_submit_rights"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:


        #xml
        xml = tracker.slots.get("xml")
        root = ET.fromstring(xml)
        
        rightsList = root.find("rightsList")

        rights = None

        if rightsList is None:
            rightsList = ET.SubElement(root, "rightsList")

        rights = xmlh.findElementWithValue(root, tracker.slots.get("rights"), "rights")

        if rights == None:
            rights = ET.SubElement(rightsList, "rights")
            rights.set('rightsURI', tracker.slots.get("rights"))
            if tracker.slots.get("isRightsName"):
                rights.text = tracker.slots.get("rightsName")

        
        root2 = copy.deepcopy(root)
        test = xmlh.ExportXML(root2)
        t = xmlh.prettify(test)

        xmlTest = {"data" : { "xml" : ET.tostring(test).decode()}}
        dispatcher.utter_custom_json(xmlTest)
        xml = ET.tostring(root).decode()
        print(t)

        return [SlotSet("xml", xml)]

class ResetNamesAction(Action):
    def name(self) -> Text:
        return "action_reset_rights"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        #resets the rights form
        return [SlotSet("rights", None), SlotSet("isRightsName", None), SlotSet("rightsName", None)]




class ValidateNames(FormValidationAction):
    def name(self) -> Text:
        return "validate_rights_form"
        
    async def required_slots(
        self,
        slots_mapped_in_domain: List[Text],
        dispatcher: "CollectingDispatcher",
        tracker: "Tracker",
        domain: "DomainDict",
    ) -> Optional[List[Text]]:
        
        additional_slots = []
        
        if tracker.slots.get("isRightsName") is True:

            additional_slots.append("rightsName")
        return additional_slots + slots_mapped_in_domain


    def validate_rightsName(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        if tracker.latest_message.get("intent")["name"] in ["question", "stop", "change", "revert"]:
            intent =  tracker.latest_message.get("intent")["name"]

            if intent == "revert":
                dispatcher.utter_message(text="Wenn du doch keine Lizenzbezeichnung hinzufügen willst, klicke jetzt einfach auf nein.")
                return {"isRightsName" : None, "rightsName" : None}

            if intent == "question":
                dispatcher.utter_message(text="bitte die genaue Bezeichnung des Lizenztyps angeben")
            return {"rightsName" : None}
        else:
            # validation failed, set this slot to None so that the
            # user will be asked for the slot again
            return {"rightsName": slot_value}

    async def extract_rightsName(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        
        if tracker.get_slot("requested_slot") == "rightsName":
            text_of_last_user_message = tracker.latest_message.get("text")

            answer = text_of_last_user_message
            
            if answer != None:
                return {"rightsName": answer}
            
            return {"rightsName" : None}
