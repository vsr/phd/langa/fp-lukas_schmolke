import xml.etree.ElementTree as ET
from xml.dom import minidom

# slot Mapping Helper for correct deleting XML Elements
SlotXMLMapping = {
    "title" : {
        "hierarchy" : 1,
        "name" : "title",
        "parent" : "titles"
    },
    "subtitle" : {
        "hierarchy" : 1,
        "name" : "title",
        "parent" : "titles"
    },
    "publisher" : {
        "hierarchy" : 0,
        "name" : "publisher"
    },
    "subject" : {
        "hierarchy" : 1,
        "name" : "subject",
        "parent" : "subjects"
    },
    "description" : {
        "hierarchy" : 1,
        "name" : "description",
        "parent" : "descriptions"
    },
    "identifier" : {
        "hierarchy" : 0,
        "name" : "identifier",
    },
    "resourceTypeDescription": {
        "hierarchy" : 0,
        "name" : "resourceType"
    },
    "format" : {
        "hierarchy" : 1,
        "name" : "format",
        "parent" : "formats"
    },
    "size" : {
        "hierarchy" : 1,
        "name" : "size",
        "parent" : "sizes"
    },
    "version" : {
        "hierarchy" : 0,
        "name" : "version",
    },
    "language" : {
        "hierarchy" : 0,
        "name" : "language",
    },
    "name" : {
        "hierarchy" : 2,
        "name" : "creatorName",
        "parent" : "creators"
    },
    "contributoName" : {
        "hierarchy" : 2,
        "name" : "contributorName",
        "parent" : "contributors"
    },
    "publishYear" : {
        "hierarchy" : 0,
        "name" : "publicationYear"
    },
    "date" : {
        "hierarchy" : 1,
        "name" : "date",
        "parent" : "dates"
    },
    "geoLocationPlace" : {
        "hierarchy" : 2,
        "name" : "geoLocationPlace",
        "parent" : "geoLocations"
    },
    "rights" : {
        "hierarchy" : 1,
        "name" : "rights",
        "parent" : "rightsList"
    }
}

class XMLHelperClass:
    """
    XML Helper class for handling all XML logic
    (generation, parsing, adding and removing elements)
    """
    def generateRootXML():
        """generate Root Element

        Returns:
            Elementtree Element: root Node
        """
        root = ET.Element("ressource")
        return root


    def prettify(elem):
        """Return a pretty-printed XML string for the Element.
        """
        rough_string = ET.tostring(elem, 'utf-8')
        reparsed = minidom.parseString(rough_string)
        return reparsed.toprettyxml(indent="  ")


    def parseFromString(string):
        """parse XML from String

        Args:
            string : XMl string

        Returns:
            ElementTree Element: XML Object
        """
        string = string.replace('\n','')
        return ET.fromstring(string)


    def ExportXML(root):
        """Export XML 

        Args:
            root (ElementTree Element): XML Object

        Returns:
            ElementTree Element: XML Object with Namespaces
        """
        root.set('xmlns', 'http://datacite.org/meta/schema/kernel-3')
        root.set('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance')
        root.set('xsi:schemaLocation', 'http://datacite.org/meta/schema/kernel-3 http://schema.datacite.org/meta/kernel-3/metadata.xsd')
        return root


    def removeElement(tree, value, slotName):
        """Head Function for removing Elements

        Args:
            tree (ElementTree tree): XML
            value (string): Value of XML Element
            slotName (string): name of Element

        Returns:
            ElementTree tree: XML Object
        """
        xmlName = SlotXMLMapping[slotName]["name"]

        #if hierarchy zero, no recursive deleting
        if not SlotXMLMapping[slotName]["hierarchy"]:
            return XMLHelperClass.removeSingleElement(tree, value, xmlName)
        else:
            return XMLHelperClass.removeElements(tree, value, xmlName, SlotXMLMapping[slotName]["parent"] )


    def removeSingleElement(tree, value, element):
        """removes Single XML Element from XML Object

        Args:
            tree (Elementtree tree): XML Object
            value (string): Value of Element
            element (string): Element Name

        Returns:
            ElementTree tree: XML Object
        """
        for parent in tree.iterfind(f'.//{element}/..'):
            for child in parent.iter(f'{element}'):
                print(child.text)
                if child.text == value:
                    parent.remove(child)    
        return tree
    

    def removeElements(tree, value, findElement, headElement):
        """removes nested XML Element from XML Object

        Args:
            tree (Elementtree tree): XML Object
            value (string): Value of Element
            findElement (): Element which will be removed
            headElement (): head Element, in which findElement is nested in

        Returns:
            ElementTree tree: XML Object
        """
        #create parent - child dict because elementtree doesnt support native child - parent connection
        parent_map = {c: p for p in tree.iter() for c in p}
        #find parent element 
        for parent in tree.iterfind(f'.//{headElement}'):
            #find subelement with value
            for child in parent.iter(f'{findElement}'):
                print(f"child: {child.text}")
                if child.text == value:
                    print(f"will remove {child.text}")
                    #go recursively higher until it reaches the parent element
                    while parent_map[child] != parent: child = parent_map[child]
                    #remove all subelements 
                    parent.remove(child) 
                    #if parent is now empty, remove also this element
                    if parent.find('*') is None:
                        help = parent_map[parent]
                        help.remove(parent)
        
        return tree
    

    def addElement(tree, childNode, headNodeName):
        """adds an Element with head Element

        Args:
            tree (ElementTree tree): XML Object
            childNode (Element): Element to be added
            headNodeName (Element): head Element

        Returns:
            [type]: [description]
        """
        if tree.find(f'.//{headNodeName}'):
            elem = tree.find(f'.//{headNodeName}')
            elem.append(childNode)
        else:
            tmp = ET.SubElement(tree, headNodeName)
            tmp.append(childNode)
    
        return tree
    

    def findElementWithValue(tree, value, Element):
        """Get Element by value and Element Name

        Args:
            tree (ElementTree tree): XML Element
            value (string): value
            Element (string): Element Name

        Returns:
            [type]: [description]
        """
        for elem in tree.iterfind(f'.//{Element}'):
            if elem.text == value:
                return elem
        return None
    

    def findElement(tree, Element):
        for parent in tree.iterfind(f'.//{Element}'):
            return parent
        return None
