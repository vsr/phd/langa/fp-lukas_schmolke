"""
Main File for doing the revert action 
"""

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import EventType, SlotSet

from .XMLHelper import XMLHelperClass as xmlh
import xml.etree.ElementTree as ET

from .XMLHelper import SlotXMLMapping


class ActionRevertLastSlot(Action):
    """
    Class for Revert Action to revert the last input 

    """

    def name(self) -> Text:
        return "action_revert"


    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        slot = None
        value = None
        for event in tracker.events[::-1]:
            
            #ignore requested slot and xml, and value != None
            if event["event"] == "slot" and event["name"] != "requested_slot" and event["value"] != None and event["name"] != "xml":
                slot = event["name"]
                value = event["value"]

                if tracker.slots.get(slot) != None:
                    break
        
        # slot contains the xml snippet
        xml = tracker.slots.get("xml")
        
        if slot:

            # mapping file, if slot is an key, than the slot needs to be removed from XML snipped
            if slot in SlotXMLMapping and xml:
                
                # convert and remove Element from xml Snipped
                root = ET.fromstring(xml)
                root = xmlh.removeElement(root, value, slot)
                xml = ET.tostring(root).decode()
                
            dispatcher.utter_message(text=f"Ok, ich setze '{value}' zurück! ⚙️📝")

            # set reverted slot to None and update xml
            return [SlotSet(slot, None), SlotSet("xml", xml)]
                
        return []


class resetRequestedSlot(Action):


    def name(self) -> Text:
        return "action_reset_requested_slot"


    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        return [SlotSet("requested_slot", None)]

