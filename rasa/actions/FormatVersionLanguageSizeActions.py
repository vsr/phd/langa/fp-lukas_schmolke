from typing import Text, List, Any, Dict

from rasa_sdk import Tracker, FormValidationAction, Action
from rasa_sdk.executor import CollectingDispatcher
from typing import Text, List, Optional
from rasa_sdk.events import EventType, SlotSet
from rasa_sdk.types import DomainDict
from rasa_sdk.forms import FormValidationAction

from typing import Dict, Text, List, Optional, Any

from .XMLHelper import XMLHelperClass as xmlh
import xml.etree.ElementTree as ET

import copy

class ActionSubmitSize(Action):
    def name(self) -> Text:
        return "action_submit_size"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        #get slots
        sizeText = tracker.slots.get("size")
        #xml

        xml = tracker.slots.get("xml")
        root = ET.fromstring(xml)
        
        sizes = xmlh.findElement(root, "sizes")
        if sizes == None:
            sizes = ET.SubElement(root, "sizes")
        
        size = xmlh.findElementWithValue(root, sizeText, "size")
        if size == None:
            size = ET.SubElement(sizes, "size").text = sizeText

 
        
        root2 = copy.deepcopy(root)
        test = xmlh.ExportXML(root2)

        xml = ET.tostring(root).decode()
        xmlTest = {"data" : { "xml" : ET.tostring(test).decode()}}
        dispatcher.utter_custom_json(xmlTest)

        dispatcher.utter_message(text=f"Deine angegebene Größe ist {sizeText} ")

        return [SlotSet("xml", xml)]

class ActionTestSubmitFormat(Action):
    def name(self) -> Text:
        return "action_submit_format"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        #get slots
        formatText = tracker.slots.get("format")
        #xml

        xml = tracker.slots.get("xml")
        root = ET.fromstring(xml)
        

        formats = xmlh.findElement(root, "formats")
        if formats == None:
            formats = ET.SubElement(root, "formats")
        
        form = xmlh.findElementWithValue(root, formatText, "format")
        if form == None:
            form = ET.SubElement(formats, "format").text = formatText

 
        tree = ET.ElementTree(root) 
        
        root2 = copy.deepcopy(root)
        test = xmlh.ExportXML(root2)

        xml = ET.tostring(root).decode()

        xmlTest = {"data" : { "xml" : ET.tostring(test).decode()}}
        dispatcher.utter_custom_json(xmlTest)
        
        dispatcher.utter_message(text=f"Deine angegebenes Format: {formatText} ")

        return [SlotSet("xml", xml)]



class ActionTestSubmitVersion(Action):
    def name(self) -> Text:
        return "action_submit_version"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        #get slots
        versionText = tracker.slots.get("version")
        #xml

        xml = tracker.slots.get("xml")
        root = ET.fromstring(xml)
        

        version = xmlh.findElementWithValue(root, versionText, "version")
        if version == None:
            version = ET.SubElement(root, "format").text = versionText

 
        tree = ET.ElementTree(root) 
        
        root2 = copy.deepcopy(root)
        test = xmlh.ExportXML(root2)

        xml = ET.tostring(root).decode()
        xmlTest = {"data" : { "xml" : ET.tostring(test).decode()}}
        dispatcher.utter_custom_json(xmlTest)

        dispatcher.utter_message(text=f"Deine Version ist {versionText} ")

        return [SlotSet("xml", xml)]


class ActionSubmitLanguage(Action):
    def name(self) -> Text:
        return "action_submit_language"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        #get slots
        languageText = tracker.slots.get("language")
        #xml

        xml = tracker.slots.get("xml")
        root = ET.fromstring(xml)
        


        form = xmlh.findElementWithValue(root, languageText, "language")
        if form == None:
            form = ET.SubElement(root, "language").text = languageText

 
        tree = ET.ElementTree(root) 
        
        root2 = copy.deepcopy(root)
        test = xmlh.ExportXML(root2)

        xml = ET.tostring(root).decode()
        xmlTest = {"data" : { "xml" : ET.tostring(test).decode()}}
        dispatcher.utter_custom_json(xmlTest)
        dispatcher.utter_message(text=f"Dein Languagecode lautet {languageText} ")

        return [SlotSet("xml", xml)]