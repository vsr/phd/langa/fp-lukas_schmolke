# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

import requests
import json

class ActionWeatherEasterEgg(Action):

    def name(self) -> Text:
        return "action_weather"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        zipcode = tracker.slots.get("plz")
        weatherplace = tracker.slots.get("weatherplace")
        r = ""

        if zipcode is not None:
            r = requests.get(f'http://api.openweathermap.org/data/2.5/weather?zip={zipcode},de&appid=703e897d921e3e72e2e527c2e63cabf9&lang=de')
        else:
            r = requests.get(f'http://api.openweathermap.org/data/2.5/weather?q={weatherplace}&appid=703e897d921e3e72e2e527c2e63cabf9&lang=de')

        parsed = json.loads(r.text)
        
        if parsed["cod"] == "400":
            dispatcher.utter_message(text="Leider hast du keine gültige Postleitzahl angegeben.")
            return []
        temp = round(int(parsed['main']['temp']) - 273.15, 1)
        print(temp)
        tempresponse = ""
        if temp <= 5:
            tempresponse = " zieh dich also warm an! ☃️"
        elif temp > 5 and temp < 20:
            tempresponse = " nimm besser eine Jacke mit :)"
        elif temp > 20 and temp < 30:
            tempresponse =" heute wird es warm! ⛅️"
        elif temp > 30:
            tempresponse =" heute wird es richtig heiß 🔥"
        print(parsed['name'])
        print(parsed['weather'][0]['description'])
        print(tempresponse, temp)
        dispatcher.utter_message(text=f"Das Wetter für {parsed['name']}: \nHeute ist es {parsed['weather'][0]['description']}, wir haben gerade {temp}°C, {tempresponse} ")

        return []
