from typing import Text, List, Any, Dict

from rasa_sdk import Tracker, FormValidationAction, Action
from rasa_sdk.executor import CollectingDispatcher
from typing import Text, List, Optional
from rasa_sdk.events import EventType, SlotSet, FollowupAction
from rasa_sdk.types import DomainDict
from rasa_sdk.forms import FormValidationAction

from typing import Dict, Text, List, Optional, Any

from .XMLHelper import XMLHelperClass as xmlh
import xml.etree.ElementTree as ET

import copy

class ActionTestSubmitNames(Action):
    def name(self) -> Text:
        return "action_submit_title"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        #get title
        title = tracker.slots.get("title")
        dispatcher.utter_message(text=f"Dein Titel lautet: {title}")

        #xml

        xml = tracker.slots.get("xml")
        root = None

        if xml == None:
            root = xmlh.generateRootXML()
        else:
            root = ET.fromstring(xml)

        #add title
        xmltitles = xmlh.findElement(root, "titles")
        if xmltitles == None:
            xmltitles = ET.SubElement(root, "titles")
        
        xmltitle = xmlh.findElementWithValue(root, title, "title")
        if xmltitle == None:
            xmltitle = ET.SubElement(xmltitles, "title").text = title
        
        if tracker.get_slot("isSubtitle"):
            subtitle = tracker.slots.get("subtitle")

            xmlsubtitle = xmlh.findElementWithValue(root, subtitle, "title")
            if xmlsubtitle == None:
                xmlsubtitle = ET.SubElement(xmltitles, "title")
                xmlsubtitle.text = subtitle
                xmlsubtitle.set('titleType', 'Subtitle')

            dispatcher.utter_message(text=f"Du hast außerdem einen Subtitel: {subtitle}")

        # add publisher
        publisher = tracker.slots.get("publisher")

        xmlpublsiher = xmlh.findElementWithValue(root, publisher, "publisher")
        if xmlpublsiher == None:
            xmlpublsiher = ET.SubElement(root, "publisher").text = publisher
        dispatcher.utter_message(text=f"Dein Publisher ist {publisher}")

        #add back to xml slot and print it beautiful
        tree = ET.ElementTree(root) 
        
        root2 = copy.deepcopy(root)
        test = xmlh.ExportXML(root2)
        t = xmlh.prettify(test)
        
        xml = ET.tostring(root).decode()
        xmlWeb = {}
        
        xmlWeb["xml"] = {"xml" : ET.tostring(test).decode()}
                
        xmlTest = {"data" : { "xml" : ET.tostring(test).decode()}}
        dispatcher.utter_custom_json(xmlTest)
        return [SlotSet("xml", xml)]



class TitleFormValidation(FormValidationAction):
    def name(self) -> Text:
        return "validate_title_form"
        
    async def required_slots(
        self,
        slots_mapped_in_domain: List[Text],
        dispatcher: "CollectingDispatcher",
        tracker: "Tracker",
        domain: "DomainDict",
    ) -> Optional[List[Text]]:
    
        additional_slots = []

        if tracker.slots.get("isSubtitle") is True:
            additional_slots.append("subtitle")

        return additional_slots + slots_mapped_in_domain


    def validate_subtitle(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate subtitle"""

        if tracker.latest_message.get("intent")["name"] in ["question", "stop", "change", "revert"]:
            intent =  tracker.latest_message.get("intent")["name"]
            if intent == "revert":
                dispatcher.utter_message(text="Okay, falls du dich vertippt hast, frage ich dich nun nocheinmal ;)")
                return {"subtitle" : None, "isSubtitle" : None}

            if intent == "question":
                dispatcher.utter_message(template="utter_explain/subtitle")
            
            return {"subtitle" : None}
        else:
            # validation failed, set this slot to None so that the
            # user will be asked for the slot again
            return {"subtitle": slot_value}


    async def extract_subtitle(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        """
        Extract subtitle directly from message
        """
        if tracker.get_slot("requested_slot") == "subtitle":

            answer = tracker.latest_message.get("text")

            if answer != None:
                return {"subtitle": answer}
            else:
                return []
