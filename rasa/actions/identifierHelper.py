import requests
from xml.etree import ElementTree
from bs4 import BeautifulSoup
import json

class identifierHelperClass:
    """
    helper class to get an Person by ISNI or ORCID ID
    """

    def __find_ISNI_name(table):
        """private helper function for extracting the name from a html table

        Args:
            table (string): html table

        Returns:
            string: name
        """

        tds = table.findAll("td")
    
        found = False
        name = ""
        for td in tds:
            if found:
                name = td.findChildren("span")[0].contents[0]
                break
            if td["class"][0] == "rec_lable":
                if td.findChildren("span")[0].contents[0] == 'Name:\xa0':
                    found = True
        return name


    def get_ISNI(id):
        """get Person from ISNI

        Args:
            id (string): isni id

        Returns:
            string: name
        """
        page = requests.get(f"https://isni.oclc.org/DB=1.2/SET=2/TTL=1/CMD?ACT=SRCH&IKT=8006&SRT=LST_nd&TRM={id}")
        soup = BeautifulSoup(page.content, 'html.parser')
        table = soup.find("table", {"summary":"title presentation"})

        if not table:
            return None
        else:
            return identifierHelperClass.__find_ISNI_name(table)

    
    def get_ORCID(id):
        """get ORCID entity by ORCID ID

        Args:
            id (string): orcid ID

        Returns:
            string: name
        """
        try:
            name = requests.get(f"https://orcid.org/{id}/person.json").json()["displayName"]
            return name
        except:
            return None