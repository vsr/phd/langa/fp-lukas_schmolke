    """
    Helper Files for Contributor Form
    """


from typing import Text, List, Any, Dict

from rasa_sdk import Tracker, FormValidationAction, Action
from rasa_sdk.executor import CollectingDispatcher
from typing import Text, List, Optional
from rasa_sdk.events import EventType, SlotSet
from rasa_sdk.types import DomainDict
from rasa_sdk.forms import FormValidationAction

from typing import Dict, Text, List, Optional, Any

from .XMLHelper import XMLHelperClass as xmlh
import xml.etree.ElementTree as ET

import copy


class ActionSubmitContributors(Action):
    """
    Class for creation and submit of xml code
    """

    def name(self) -> Text:
        return "action_submit_contributor"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        if tracker.slots.get("isContributor") is False:
            dispatcher.utter_message(
                text=f"Du hast keine Mitwirkenden an deinem Projekt!")
            return []

        contributorType = tracker.slots.get("contributorType")
        contributorName = tracker.slots.get("contributorName")
        ContributorIdentifierType = None
        contributorIdentifier = None

        dispatcher.utter_message(
            text=f"Dein Mitwirkender lautet {contributorName} als {contributorType}.")

        if tracker.slots.get("isContributorIdentifier") is True:
            ContributorIdentifierType = tracker.slots.get(
                "contributorIdentifierType")
            contributorIdentifier = tracker.slots.get("contributorIdentifier")
            dispatcher.utter_message(
                text=f"Er besitzt den Identifier {ContributorIdentifierType}: {contributorIdentifier}")

        xml = tracker.slots.get("xml")
        root = ET.fromstring(xml)

        contributors = root.find("contributors")

        contributor = None

        if contributors is None:
            contributors = ET.SubElement(root, "contributors")

        contributor = xmlh.findElementWithValue(
            root, contributorName, "contributorName")
        if contributor == None:
            contributor = ET.SubElement(contributors, "contributor")

            contributor.set('contributorType', str(contributorType))

            contributorName = ET.SubElement(
                contributor, "contributorName").text = contributorName

            # is there an identifier for the contributor, add this information also
            if ContributorIdentifierType is not None:
                if contributorType == "ISNI":
                    contributorTypeXML = ET.SubElement(
                        contributor, "nameIdentifier", nameIdentifierScheme=ContributorIdentifierType, schemeURI="http://www.isni.org").text = contributorIdentifier
                elif contributorType == "ORCID":
                    contributorTypeXML = ET.SubElement(
                        contributor, "nameIdentifier", nameIdentifierScheme=ContributorIdentifierType, schemeURI="http://orcid.org").text = contributorIdentifier

        root2 = copy.deepcopy(root)
        test = xmlh.ExportXML(root2)

        xml_payload = {"data": {"xml": ET.tostring(test).decode()}}
        dispatcher.utter_custom_json(xml_payload)

        xml = ET.tostring(root).decode()

        return [SlotSet("xml", xml)]


class ResetNamesAction(Action):
    def name(self) -> Text:
        return "action_reset_contributor"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        # resets the name form
        return [SlotSet("contributorType", None), SlotSet("contributorName", None), SlotSet("isContributorIdentifier", None), SlotSet("contributorIdentifierType", None), SlotSet("contributorIdentifier", None)]


class ValidateContributors(FormValidationAction):
    def name(self) -> Text:
        return "validate_contributor_form"

    async def required_slots(
        self,
        slots_mapped_in_domain: List[Text],
        dispatcher: "CollectingDispatcher",
        tracker: "Tracker",
        domain: "DomainDict",
    ) -> Optional[List[Text]]:

        additional_slots = []

        if tracker.slots.get("isContributor") is True:
            additional_slots.append("contributorType")
            additional_slots.append("contributorName")
            additional_slots.append("isContributorIdentifier")

            if tracker.slots.get("isContributorIdentifier") is True:
                additional_slots.append("contributorIdentifierType")
                additional_slots.append("contributorIdentifier")

        return additional_slots + slots_mapped_in_domain


    def validate_contributorType(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """
        Validation function for contributor Type
        """

        if tracker.latest_message.get("intent")["name"] in ["question", "stop", "change", "revert", "explain"]:
            intent = tracker.latest_message.get("intent")["name"]
            if intent == "revert":
                dispatcher.utter_message(
                    text="Alles klar, falls du doch keine Mitwirkenden hast, kannst du dies jetzt angeben.")
                return {"contributorType": None, "isContributor": None}

            if intent == "question":
                dispatcher.utter_message(
                    template="utter_explain/contributorType")

            return {"contributorType": None}
        else:
            if slot_value == None:
                dispatcher.utter_message(
                    text=f"Dieser Typ wird nicht unterstützt. Bitte erneut eingeben.")
            return {"contributorType": slot_value}

    async def extract_contributorType(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        if tracker.get_slot("requested_slot") == "contributorType":
            # first try to find suiting entity
            entities = tracker.latest_message["entities"]

            contributor = None

            # find entity 'contributorType' in entities from last message
            contributorEntities = [
                out for out in tracker.latest_message["entities"] if out["entity"] == "contributorType"]

            if contributorEntities:
                contributor = contributorEntities[0]["value"]

            # now check if there is already a name inside
            names = [out for out in tracker.latest_message["entities"]
                     if out["entity"] == "PER"]
            name = None

            if names:
                name = names[0]["value"]
                dispatcher.utter_message(
                    f"Du hast einen Mitwirkendenden mit dem Namen {name} angegeben")

            answer = tracker.latest_message.get("text")

            # now check manually if not already set
            if contributor == None:
                allowed_tags = ["ContactPerson", "DataCollector", "DataCurator", "DataManager", "Distributor", "Editor", "Funder", "Hostingestitution", "Producer", "ProjectLeader", "ProjectManager",
                                "ProjectMember", "RegistrationAgency", "RegistrationAuthority", "RelatedPerson", "Researcher", "ResearchGroup", "RightsHolder", "Sponsor", "Supervisor", "WorkPackageLeader", "Other"]

                contributorType = [
                    elem for elem in allowed_tags if(elem in answer)]

                if not contributorType:
                    answer = None
                    return {"contributorType": None, "contributorName": name}
                else:
                    return {"contributorType": contributorType[0], "contributorName": name}
            else:
                return {"contributorType": contributor, "contributorName": name}


    def validate_contributorName(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """
        Validation function for contributorName
        """
  
        if tracker.latest_message.get("intent")["name"] in ["question", "stop", "change", "revert", "explain"]:
            intent = tracker.latest_message.get("intent")["name"]
            if intent == "revert":
                old = tracker.slots.get("contributorType")
                dispatcher.utter_message(
                    text=f"Alles klar, ich setzte den Typ {old} zurück.")
                return {"contributorType": None}

            if intent == "question":
                dispatcher.utter_message(
                    text="Du weißt doch ganz genau was ein Name ist ;)")

            return {"contributorName": None}
        else:
            if slot_value == None:
                dispatcher.utter_message(
                    text=f"Dieser Typ wird nicht unterstützt. Bitte erneut eingeben.")
            return {"contributorName": slot_value}


    async def extract_contributorName(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        """
        Extractor function for contributorName
        """

        if tracker.get_slot("requested_slot") == "contributorName":
            name = next(tracker.get_latest_entity_values("PER"), None)

            if name:
                return {"contributorName": name}
            else:
                text_of_last_user_message = tracker.latest_message.get("text")

                return {"contributorName": text_of_last_user_message}

    def validate_isContributorIdentifier(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """
        Validation function for ContributorIdentifier
        """

        if tracker.latest_message.get("intent")["name"] in ["question", "stop", "change", "revert", "explain"]:
            intent = tracker.latest_message.get("intent")["name"]
            
            if intent == "revert":
                dispatcher.utter_message(
                    text=f"Ich setze nun den Namen {tracker.slots.get('contributorName')} zurück.")
                return {"contributorName": None, "isContributorIdentifier": None}

            if intent == "question":
                dispatcher.utter_message(
                    text="Ähnlich wie bei den anderen Identifikatoren, ist hier die Frage ob es einen eindeutigen Identifier gibt.")

            return {"isContributorIdentifier": None}
        else:
            if slot_value == None:
                dispatcher.utter_message(
                    text=f"Das hat nicht geklappt, bitte antworte mit Ja oder nein.")
            return {"isContributorIdentifier": slot_value}


    async def extract_isContributorIdentifier(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        """
        Extractor function for isContributorIdentifier
        """

        if tracker.get_slot("requested_slot") == "isContributorIdentifier":

            answer = tracker.latest_message.get("intent")

            if answer["name"] == "affirm":
                return {"isContributorIdentifier": True}
            elif answer["name"] == "deny":
                return {"isContributorIdentifier": False}
            else:
                return {"isContributorIdentifier": None}


    def validate_contributorIdentifierType(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """
        validator Function for contributorIdentifierType
        """

        if tracker.latest_message.get("intent")["name"] in ["question", "stop", "change", "revert", "explain"]:
            intent = tracker.latest_message.get("intent")["name"]

            if intent == "revert":
                dispatcher.utter_message(
                    text=f"Ok, falls du doch keinen Identifier angeben willst, kannst du dich nun korrigieren.")
                return {"isContributorIdentifier": None, "contributorIdentifierType": None}

            if intent == "question":
                dispatcher.utter_message(
                    text="Bitte Frage nach einem konkreten Typ, z.B.")

            return {"contributorIdentifierType": None}

        else:
            if slot_value == None:
                dispatcher.utter_message(
                    text=f"Das hat nicht geklappt, bitte antworte mit Ja oder nein.")
            return {"contributorIdentifierType": slot_value}


    async def extract_contributorIdentifierType(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        """
        Extractor function for contributorIdentifierType
        """

        if tracker.get_slot("requested_slot") == "contributorIdentifierType":
            answer = next(tracker.get_latest_entity_values(
                "contributorIdentifierType"), None)

            if answer != None:
                return {"contributorIdentifierType": answer}
            else:
                return {"contributorIdentifierType": None}


    def validate_contributorIdentifier(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """
        Validator function for contributorIdentifier
        """

        if tracker.latest_message.get("intent")["name"] in ["question", "stop", "change", "revert", "explain"]:
            intent = tracker.latest_message.get("intent")["name"]

            if intent == "revert":
                dispatcher.utter_message(
                    text=f"Gut, ich setzte den Identifikationstyp {tracker.slots.get('contributorIdentifierType')} zurück.")
                return {"contributorIdentifierType": None, "contributorIdentifier": None}

            if intent == "question":
                dispatcher.utter_message(
                    text=f"Hier musst du nun den Identifikator vom Typ {tracker.slots.get('contributorIdentifierType')} angeben")

            return {"contributorIdentifierType": None}
        else:
            if slot_value == None:
                dispatcher.utter_message(
                    text=f"Das hat nicht geklappt, bitte antworte mit Ja oder nein.")
            return {"contributorIdentifierType": slot_value}


    async def extract_contributorIdentifier(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        """
        Extractor function for contributorIdentifier
        """

        if tracker.get_slot("requested_slot") == "contributorIdentifier":
            text_of_last_user_message = tracker.latest_message.get("text")

            return {"contributorIdentifier": text_of_last_user_message}
