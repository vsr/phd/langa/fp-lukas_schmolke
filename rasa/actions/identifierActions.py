"""
Helper function for identifier form
"""

from typing import Text, List, Any, Dict

from rasa_sdk import Tracker, FormValidationAction, Action
from rasa_sdk.executor import CollectingDispatcher
from typing import Text, List, Optional
from rasa_sdk.events import EventType, SlotSet
from rasa_sdk.types import DomainDict
from rasa_sdk.forms import FormValidationAction

from typing import Dict, Text, List, Optional, Any

from .XMLHelper import XMLHelperClass as xmlh
import xml.etree.ElementTree as ET

from bs4 import BeautifulSoup
import requests
import copy

class ActionTestSubmitNames(Action):
    def name(self) -> Text:
        return "action_submit_identifier"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        idtype = tracker.slots.get("identifier_type")
        identifier = tracker.slots.get("identifier")

        xml = tracker.slots.get("xml")
        root = ET.fromstring(xml)


        identifierXML = xmlh.findElementWithValue(root, identifier, "identifier")
        if identifierXML == None:
            identifierXML = ET.SubElement(root, "identifier")

            # if DOi is given as html, remove html prefix
            if idtype == "DOI" and identifier.startswith("https://") or identifier.startswith("http://"):
                helper = identifier.split('/')
                identifier = "".join(helper[3:])

            identifierXML.text = identifier
            identifierXML.set('identifierType', str(idtype))

        
        root2 = copy.deepcopy(root)
        test = xmlh.ExportXML(root2)
        
        xmlTest = {"data" : { "xml" : ET.tostring(test).decode()}}
        dispatcher.utter_custom_json(xmlTest)
        
        xml = ET.tostring(root).decode()

        #print(t)
        
        dispatcher.utter_message(text=f"Du hast den Identifier {idtype}: {identifier} gewählt.")

        return [SlotSet("xml", xml)]


class identifierFormValidation(FormValidationAction):
    def name(self) -> Text:
        return "validate_identifier_form"

    def validate_identifier(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """
        Validator function for identifier
        """

        if tracker.latest_message.get("intent")["name"] in ["question", "stop", "change", "revert"]:
            intent =  tracker.latest_message.get("intent")["name"]
            if intent == "revert":
                dispatcher.utter_message(text="Okay, falls du dich vertippt hast, frage ich dich nun nocheinmal ;)")
                return {"identifier_type" : None, "identifier" : None}

            if intent == "question":
                dispatcher.utter_message(template="utter_explain/identifier")
            
            return {"identifier" : None}
        
        else:
            if tracker.slots.get("identifier_type") == "URL":
                
                # prevent error caused by possible array (extracted with multiple Classifiers, e.g. Regex and DIET)
                if isinstance(slot_value, list):
                    slot_value = slot_value[0]

                page = requests.get(slot_value)
                soup = BeautifulSoup(page.content, 'html.parser')
                volume = soup.findAll("title")[0].string

                if soup.findAll("meta", {"property" : "og:image"}):
                    message = {
                        "data" : 
                        {
                        "attachment":
                            {
                            "type":"image",
                            "payload":
                                {
                                "title":volume,
                                "src": soup.findAll("meta", {"property" : "og:image"})[0]["content"]
                                }
                            }
                        }
                    }
                    dispatcher.utter_custom_json(message)
                else:
                    dispatcher.utter_message(text=f"Deine URL verlinkt auf {volume}")
                
                return {"identifier": slot_value}

            if tracker.slots.get("identifier_type") == "DOI":
                
                # prevent error caused by possible array (extracted with multiple Classifiers, e.g. Regex and DIET)
                if isinstance(slot_value, list):
                    slot_value = slot_value[0]
                
                if not slot_value.startswith('https://'):
                    tmp = "https://doi.org/" + slot_value
                else:
                    tmp = slot_value


                page = requests.get(tmp)
                soup = BeautifulSoup(page.content, 'html.parser')
                volume = soup.findAll("title")[0].string
                dispatcher.utter_message(text=f"Deine DOI verlinkt auf {volume}")

            return {"identifier": slot_value}