from typing import Text, List, Any, Dict

from rasa_sdk import Tracker, FormValidationAction, Action
from rasa_sdk.executor import CollectingDispatcher
from typing import Text, List, Optional
from rasa_sdk.events import EventType, SlotSet
from rasa_sdk.types import DomainDict
from rasa_sdk.forms import FormValidationAction

from typing import Dict, Text, List, Optional, Any

from .XMLHelper import XMLHelperClass as xmlh
import xml.etree.ElementTree as ET

import copy

class ActionTestSubmitNames(Action):
    def name(self) -> Text:
        return "action_submit_dates"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        #get publish date
        publish = tracker.slots.get("publishYear")
        dispatcher.utter_message(text=f"Veröffentlichung: {publish}")

        #xml
        xml = tracker.slots.get("xml")

        root = ET.fromstring(xml)
        
        #only add publishdate if it is not already there
        xmlPublish = root.find("publicationYear")

        if xmlPublish is None:
            xmlPublish = ET.SubElement(root, "publicationYear")
            xmlPublish.text = publish
        
        xmldates = root.find("dates")

        if xmldates is None:
            xmldates = ET.SubElement(root, "dates")
        
        dateType = tracker.slots.get("dateType")

        if dateType is not None:
            date = xmlh.findElementWithValue(root, tracker.slots.get("date"), "date")
            if date == None:
                date = ET.SubElement(xmldates, "date", dateType=str(dateType)).text = str(tracker.slots.get("date"))
                dispatcher.utter_message(text=f"{dateType} : {tracker.slots.get('date')}")
        
        root2 = copy.deepcopy(root)
        test = xmlh.ExportXML(root2)

        xmlTest = {"data" : { "xml" : ET.tostring(test).decode()}}
        dispatcher.utter_custom_json(xmlTest)
        
        xml = ET.tostring(root).decode()

        return [SlotSet("xml", xml)]

class ResetNamesAction(Action):
    def name(self) -> Text:
        return "action_reset_date"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        return [SlotSet("dateType", None), SlotSet("date", None)]


class ValidateNames(FormValidationAction):
    def name(self) -> Text:
        return "validate_date_form"
        
    async def required_slots(
        self,
        slots_mapped_in_domain: List[Text],
        dispatcher: "CollectingDispatcher",
        tracker: "Tracker",
        domain: "DomainDict",
    ) -> Optional[List[Text]]:
        
        additional_slots = []
        
        if tracker.slots.get("moreDates") is True:

            additional_slots.append("dateType")
            additional_slots.append("date")
        return additional_slots + slots_mapped_in_domain


    def validate_dateType(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate cuisine value."""

        if tracker.latest_message.get("intent")["name"] in ["question", "stop", "change", "revert"]:
            return {"dateType" : None}
        else:
            # validation failed, set this slot to None so that the
            # user will be asked for the slot again
            return {"dateType": slot_value}



    async def extract_dateType(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        
        #extract dateType from entity
        text_of_last_user_message = tracker.latest_message.get("text")

        if tracker.get_slot("requested_slot") == "dateType":
            answer = next(tracker.get_latest_entity_values("dateType"), None)
            
            print(answer)
            
            if answer != None:
                return {"dateType": answer}

    def validate_date(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate cuisine value."""

        if tracker.latest_message.get("intent")["name"] in ["question", "stop", "change", "revert"]:
            return {"date" : None}
        else:
            # validation failed, set this slot to None so that the
            # user will be asked for the slot again
            return {"date": slot_value}


    async def extract_date(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        #extract date from entity
        if tracker.get_slot("requested_slot") == "date":

            answer = next(tracker.get_latest_entity_values("date"), None)

            if answer != None:
                return {"date": answer}
            else:
                return {"date" : None}