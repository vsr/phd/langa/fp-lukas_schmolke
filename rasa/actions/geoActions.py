from typing import Text, List, Any, Dict

from rasa_sdk import Tracker, FormValidationAction, Action
from rasa_sdk.executor import CollectingDispatcher
from typing import Text, List, Optional
from rasa_sdk.events import EventType, SlotSet
from rasa_sdk.types import DomainDict
from rasa_sdk.forms import FormValidationAction

from typing import Dict, Text, List, Optional, Any

from .XMLHelper import XMLHelperClass as xmlh
import xml.etree.ElementTree as ET

import copy
import requests
import json
import urllib.parse

class ActionXMLDescriptionSubmitter(Action):
    def name(self) -> Text:
        return "action_submit_geo"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        """
        function for generating xml snipped with geo Data
        """

        #all slots
        geoLocationPlace = tracker.slots.get("geoLocationPlace")
        geoLocationPoint = tracker.slots.get("geoLocationPoint")

        #xml
        xml = tracker.slots.get("xml")
        root = ET.fromstring(xml)
        
        descriptions = root.find("geoLocations")

        description = None

        if descriptions is None:
            geoLocations = ET.SubElement(root, "geoLocations")

        
        geoLocation = xmlh.findElementWithValue(root, geoLocationPlace, "geoLocationPlace")
        if geoLocation == None:
            geoLocation = ET.SubElement(descriptions, "geoLocation")

            geoLocationPlaceXML = ET.SubElement(geoLocation, "geoLocationPlace").text = geoLocationPlace
            geoLocationPointXML = ET.SubElement(geoLocation, "geoLocationPoint").text = geoLocationPoint

        tree = ET.ElementTree(root) 
        
        root2 = copy.deepcopy(root)
        test = xmlh.ExportXML(root2)
        t = xmlh.prettify(test)

        xmlTest = {"data" : { "xml" : ET.tostring(test).decode()}}
        dispatcher.utter_custom_json(xmlTest)
        xml = ET.tostring(root).decode()
        print(t)

        return [SlotSet("xml", xml)]

class ResetNamesAction(Action):
    def name(self) -> Text:
        return "action_reset_geo"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        """
        reset all geo data slots
        """

        #resets the description form
        return [SlotSet("geoLocationPlace", None), SlotSet("geoLocationPoint", None)]


class ValidateNames(FormValidationAction):
    def name(self) -> Text:
        return "validate_geo_form"
        
    async def required_slots(
        self,
        slots_mapped_in_domain: List[Text],
        dispatcher: "CollectingDispatcher",
        tracker: "Tracker",
        domain: "DomainDict",
    ) -> Optional[List[Text]]:

        return slots_mapped_in_domain


    def validate_geoLocationPlace(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """
        Validator function for geoLocationPlace

        function handles a User inpot which can be Coordinates or also an adress and creates an google maps Link to 
        check if the Geoplace is valid
        """
        if tracker.latest_message.get("intent")["name"] in ["question", "stop", "change", "revert"]:
            intent =  tracker.latest_message.get("intent")["name"]

            if intent == "revert":
                dispatcher.utter_message(text=f"Alles klar, ich setze  {slot_value} zurück.")
                return {"geoLocationPoint" : None, "geoLocationPlace" : None}

            if intent == "question":
                dispatcher.utter_message(template="utter_explain/geoLocationPlace")
                return {"geoLocationPlace" : None}
        else:
            if tracker.latest_message.get("intent")["name"] == "say_coordinates":
                coords = tracker.latest_message["entities"]

                coords = [out for out in tracker.latest_message["entities"] if out["entity"] == "coordinates"]
                longi = lat = 0
                for elem in coords:
                    if elem["extractor"] == "DIETClassifier":
                        if elem["role"] == "lat":
                            lat = elem["value"]
                        elif elem["role"] == "long":
                            longi = elem["value"]
                    if longi == 0:
                        longi = coords[1]["value"]
                        lat = coords[0]["value"]
                    
                point = str(lat) + " - " + str(longi)
                dispatcher.utter_message(text=f"Okay, ich habe das  [hier](http://www.google.com/maps/place/{lat},{longi} gefunden")
                dispatcher.utter_message(text="Jetzt benötige ich noch eine passende Beschreibung für deine Koordinaten.")
                return {"geoLocationPoint" : point, "geoLocationPlace" : None}

            # if no coordinates are given  
            elif tracker.slots.get("geoLocationPoint") is None:
                encoded = urllib.parse.quote_plus(slot_value)
                print(encoded)
                r = requests.get(f'http://api.positionstack.com/v1/forward?access_key=25d1809e7b4cb1c944b5a6e2ad7c9c18&query={encoded}')
                print(r.text)
                parsed = json.loads(r.text)
                if not parsed:
                    dispatcher.utter_message(text=f"deine Adresse {slot_value} ist ungültig. Bitte nocheinmal ;)")
                    return {"geoLocationPlace" : None}
                point = ""
                point += str(parsed["data"][0]["latitude"]) + " - " + str(parsed["data"][0]["longitude"])
                dispatcher.utter_message(text=f"Okay, ich habe folgenden Platz gefunden [hier](http://www.google.com/maps/place/{parsed['data'][0]['latitude']},{parsed['data'][0]['longitude']})")
                return {"geoLocationPlace": slot_value, "geoLocationPoint" : point}


    async def extract_geoLocationPlace(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        """
        Extractor function for geoLocationPlace 
        """
        
        if tracker.get_slot("requested_slot") == "geoLocationPlace":
            answer = next(tracker.get_latest_entity_values("geoLocationPlace"), None)

            if answer:
                return {"geoLocationPlace": answer}
            else:
                text_of_last_user_message = tracker.latest_message.get("text")

            answer = text_of_last_user_message
            
            if answer != None:
                return {"geoLocationPlace": answer}
            
            return {"geoLocationPlace" : None}
