from typing import Text, List, Any, Dict

from rasa_sdk import Tracker, FormValidationAction, Action
from rasa_sdk.executor import CollectingDispatcher
from typing import Text, List, Optional
from rasa_sdk.events import EventType, SlotSet
from rasa_sdk.types import DomainDict
from rasa_sdk.forms import FormValidationAction

from typing import Dict, Text, List, Optional, Any

from .XMLHelper import XMLHelperClass as xmlh
import xml.etree.ElementTree as ET

import copy



class ActionTestSubmitNames(Action):
    def name(self) -> Text:
        return "action_submit_subject"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        subject = tracker.slots.get("subject")
        dispatcher.utter_message(text=f"Subject: {subject}")

        #xml
        xml = tracker.slots.get("xml")

        root = ET.fromstring(xml)
        
        #only add publishdate if it is not already there
        xmlSubject = root.find("subjects")

        if xmlSubject is None:
            xmlSubject = ET.SubElement(root, "subjects")
        
        subj = xmlh.findElementWithValue(root, subject, "subject")
        print(subj)
        if subj == None:
            subj = ET.SubElement(xmlSubject, "subject")
            dewey = ""
            if tracker.slots.get("isDewey"):
                subj.set("subjectScheme", "DDC")
                subj.set("schemeURI", "http://dewey.info/")
                dewey = tracker.slots.get("deweyCode")
                dispatcher.utter_message(text=f"Dewey Code: {dewey}")
            if dewey:
                subj.text = dewey + " " + subject
            else:
                subj.text = subject

        tree = ET.ElementTree(root) 
        
        root2 = copy.deepcopy(root)
        test = xmlh.ExportXML(root2)
        t = xmlh.prettify(test)

        xml = ET.tostring(root).decode()
        print(t)

        xmlTest = {"data" : { "xml" : ET.tostring(test).decode()}}
        dispatcher.utter_custom_json(xmlTest)
 
        return [SlotSet("xml", xml)]

class ResetNamesAction(Action):
    def name(self) -> Text:
        return "action_reset_subject"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        return [SlotSet("subject", None), SlotSet("isDewey", None), SlotSet("deweyCode", None)]


class ValidateNames(FormValidationAction):
    def name(self) -> Text:
        return "validate_subject_form"
        
    async def required_slots(
        self,
        slots_mapped_in_domain: List[Text],
        dispatcher: "CollectingDispatcher",
        tracker: "Tracker",
        domain: "DomainDict",
    ) -> Optional[List[Text]]:
        
        additional_slots = []
        
        if tracker.slots.get("isDewey") is True:

            additional_slots.append("deweyCode")
        return additional_slots + slots_mapped_in_domain


    def validate_deweyCode(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """
        Validate DeweyCode
        """

        if tracker.latest_message.get("intent")["name"] in ["question", "stop", "change", "revert"]:
            intent =  tracker.latest_message.get("intent")["name"]

            if intent == "revert":
                dispatcher.utter_message(text="Kein Problem, wenn du doch keinen Dewey Code hast, dann kannst du ihn jetzt überspringen.")
                return {"deweyCode" : None, "isDewey" : None}

            if intent == "question":
                dispatcher.utter_message(template="utter_explain/deweyCode")
            return {"deweyCode" : None}
        else:
            return {"deweyCode": slot_value}


    async def extract_deweyCode(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        # extract dewey Code
        if tracker.get_slot("requested_slot") == "deweyCode":
            answer = next(tracker.get_latest_entity_values("deweyNumber"), None)
            
            if answer != None:
                return {"deweyCode": answer}
            
            return {"deweyCode" : None}
