from typing import Text, List, Any, Dict

from rasa_sdk import Tracker, FormValidationAction, Action
from rasa_sdk.executor import CollectingDispatcher
from typing import Text, List, Optional
from rasa_sdk.events import EventType, SlotSet
from rasa_sdk.types import DomainDict
from rasa_sdk.forms import FormValidationAction

from typing import Dict, Text, List, Optional, Any

from .XMLHelper import XMLHelperClass as xmlh
import xml.etree.ElementTree as ET

import copy

class ActionXMLDescriptionSubmitter(Action):
    def name(self) -> Text:
        return "action_xml_description"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        """
        submit function for generating xml snipped for descriptions
        """

        #all slots
        descriptType = tracker.slots.get("descriptionType")
        descript = tracker.slots.get("description")

        #xml
        xml = tracker.slots.get("xml")
        root = ET.fromstring(xml)
        
        descriptions = root.find("descriptions")

        description = None

        if descriptions is None:
            descriptions = ET.SubElement(root, "descriptions")

        description = xmlh.findElementWithValue(root, descript, "description")
        if description == None:
            description = ET.SubElement(descriptions, "description")

            description.set('descriptionType', str(descriptType))
            description.text = descript


        
        root2 = copy.deepcopy(root)
        test = xmlh.ExportXML(root2)

        xmlTest = {"data" : { "xml" : ET.tostring(test).decode()}}
        dispatcher.utter_custom_json(xmlTest)
        xml = ET.tostring(root).decode()

        return [SlotSet("xml", xml)]



class ActionSubmitDescription(Action):
    def name(self) -> Text:
        return "action_submit_description"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        """
        only submit function with mapping for german word
        """


        descriptionType = tracker.slots.get("descriptionType")
        description = tracker.slots.get("description")

        helper = {"Abstract" : "Abstract", 
                  "Methods" : "Methoden",
                  "SeriesInformation" : "Series Information",
                  "TableOfContents" : "Inhaltsverzeichnis",
                  "Other" : "Sonstige"
                  }
        dispatcher.utter_message(text=f"Du hast eine Beschreibung vom Typ {helper[descriptionType]} gewählt. Deine Beschreibung lautet: ")
        dispatcher.utter_message(text=f"{description}")

class ResetNamesAction(Action):
    def name(self) -> Text:
        return "action_reset_description"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:

        #resets the description form
        return [SlotSet("description", None), SlotSet("descriptionType", None)]
